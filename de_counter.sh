#!/bin/bash

# Stijn Wijnen
# s3515281

FILE=$1

# counts the occurrences of the word 'de' (case insensitive).
function de_counter {
    cat $FILE | grep -Eoh '\w+' | grep -wic 'de'
}

de_counter
